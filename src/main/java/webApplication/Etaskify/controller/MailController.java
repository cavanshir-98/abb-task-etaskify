package webApplication.Etaskify.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import webApplication.Etaskify.service.MailService;

@RequiredArgsConstructor
@RestController
@RequestMapping("/mail")
public class MailController {

    private final MailService service;

    @GetMapping
    void sendEmail() {
        service.sendWithoutAttachment("mhuseinov7@gmail.com", "a.rahib15@gmail.com", "test");
    }
}
