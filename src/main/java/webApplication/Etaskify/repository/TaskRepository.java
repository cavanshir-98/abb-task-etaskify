package webApplication.Etaskify.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import webApplication.Etaskify.model.Organization;
import webApplication.Etaskify.model.Task;
import webApplication.Etaskify.resource.organization.OrganizationSearchRequest;
import webApplication.Etaskify.resource.task.TaskSearchRequest;
import webApplication.Etaskify.resource.task.TaskSearchResponseInfoDto;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

public interface TaskRepository extends JpaRepository<Task, Long> , JpaSpecificationExecutor<Task> {

    Optional<Task> findByTitle(String title);

    default Page<Task> findAll(TaskSearchRequest searchReq) {
        return findAll((root, query, cb) -> {
           List< Predicate >predicate = new ArrayList<>();

            if (searchReq.getId() != null) {
                predicate.add((cb.equal(root.get(Task.Fields.id), searchReq.getId())));
            }
            if (searchReq.getName() != null) {
                predicate .add(cb.like(cb.lower(root.get(Task.Fields.description)),"%"+searchReq.getName().toLowerCase()+"%"));
            }

            return cb.and(predicate.toArray(new Predicate[0]));
        }, PageRequest.of(searchReq.getPage(), searchReq.getSize(), Sort.by(Sort.Direction.valueOf(searchReq.getDir()), searchReq.getOrder())));
    }
}
