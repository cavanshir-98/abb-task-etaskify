package webApplication.Etaskify.resource.organization;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class OrganizationSearchRequest extends PageParams {

    private Long id;
    private String name;
}
