package webApplication.Etaskify.resource.task;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import webApplication.Etaskify.enums.TaskStatus;

import javax.persistence.Column;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.time.LocalDate;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TaskCreateRequestDto {

    @NotBlank
    private String title;

    @NotBlank
    private String description;

    @Valid
    private TaskStatus status;

    @Column(name = "deadline", nullable = false)
    private LocalDate deadline;

    private Long organizationId;
}
