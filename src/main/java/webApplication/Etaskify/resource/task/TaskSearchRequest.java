package webApplication.Etaskify.resource.task;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import webApplication.Etaskify.resource.organization.PageParams;

@Data
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class TaskSearchRequest extends PageParams {

    private Long id;
    private String name;
}
