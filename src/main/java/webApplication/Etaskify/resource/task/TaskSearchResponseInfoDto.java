package webApplication.Etaskify.resource.task;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import webApplication.Etaskify.model.Task;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TaskSearchResponseInfoDto {

    private Long id;
    private String name;

    public static TaskSearchResponseInfoDto getDto(Task task) {
        return TaskSearchResponseInfoDto.builder()
                .id(task.getId())
                .name(task.getDescription())
                .build();
    }
}
