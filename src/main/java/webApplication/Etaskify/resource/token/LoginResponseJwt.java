package webApplication.Etaskify.resource.token;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class LoginResponseJwt {

    String jwt;
}
