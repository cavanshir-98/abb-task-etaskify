package webApplication.Etaskify.service;

public interface MailService {

    void sendWithoutAttachment(String to, String sender, String text);
}
