package webApplication.Etaskify.service;


import org.springframework.data.domain.Page;
import webApplication.Etaskify.resource.task.TaskCreateRequestDto;
import webApplication.Etaskify.resource.task.TaskResponseDto;
import webApplication.Etaskify.resource.task.TaskSearchRequest;
import webApplication.Etaskify.resource.task.TaskSearchResponseInfoDto;

public interface TaskService {

    TaskResponseDto create(TaskCreateRequestDto requestDto);

    void taskAddForUser(Long taskId, Long userid);

    void delete(Long id);

    Page<TaskSearchResponseInfoDto> list(TaskSearchRequest searchRequest);
}
