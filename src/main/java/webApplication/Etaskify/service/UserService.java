package webApplication.Etaskify.service;

import webApplication.Etaskify.resource.user.RegisterUserDto;

public interface UserService {

    void create(RegisterUserDto dto);
}
