package webApplication.Etaskify.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.modelmapper.ModelMapper;
import webApplication.Etaskify.service.MailService;
import webApplication.Etaskify.service.TaskService;
import webApplication.Etaskify.exception.NameMustBeUniqueException;
import webApplication.Etaskify.exception.TaskNotFoundException;
import webApplication.Etaskify.exception.organization.OrganizationNotFoundException;
import webApplication.Etaskify.model.Organization;
import webApplication.Etaskify.model.Task;
import webApplication.Etaskify.model.User;
import webApplication.Etaskify.repository.OrganizationRepository;
import webApplication.Etaskify.repository.TaskRepository;
import webApplication.Etaskify.repository.UserRepository;
import webApplication.Etaskify.resource.task.TaskCreateRequestDto;
import webApplication.Etaskify.resource.task.TaskResponseDto;
import webApplication.Etaskify.resource.task.TaskSearchRequest;
import webApplication.Etaskify.resource.task.TaskSearchResponseInfoDto;

import javax.transaction.Transactional;

@Slf4j
@Service
@RequiredArgsConstructor
public class TaskServiceImpl implements TaskService {

    private final ModelMapper modelMapper;
    private final TaskRepository taskRepository;
    private final UserRepository userRepository;
    private final OrganizationRepository organizationRepository;
    private final MailService mailService;

    @Override
    @Transactional
    public TaskResponseDto create(TaskCreateRequestDto requestDto) {
        Task task = new Task();
        taskRepository.findByTitle(requestDto.getTitle())
                .ifPresent(organization -> {
                    throw new NameMustBeUniqueException(requestDto.getTitle());
                });
        task.setTitle(requestDto.getTitle());
        task.setDescription(requestDto.getDescription());
        task.setDeadline(requestDto.getDeadline());
        task.setStatus(requestDto.getStatus());
        task.setOrganization(getOrganisation(requestDto.getOrganizationId()));
        return modelMapper.map(taskRepository.save(task), TaskResponseDto.class);
    }

    @Override
    public void taskAddForUser(Long taskId, Long userid) {
        Task task1 = taskRepository.getReferenceById(taskId);
        User user = userRepository.getReferenceById(userid);
        task1.getAssignee().add(user);
        userRepository.save(user);
        mailService.sendWithoutAttachment(user.getEmail(), "cavansir.asad@gmail.com", "{} is assigned to you. " + task1.getTitle());
    }

    @Override
    @Transactional
    public void delete(Long id) {
        log.trace("Remove task with id {}", id);
        taskRepository.delete(taskRepository.findById(id)
                .orElseThrow(() -> new TaskNotFoundException(id)));
    }

    @Override
    public Page<TaskSearchResponseInfoDto> list(TaskSearchRequest searchRequest) {
        if (searchRequest.getOrder()==null){
            searchRequest.setOrder("id");
        }
        Page<Task> organizations = taskRepository.findAll(searchRequest);
        return organizations.map(TaskSearchResponseInfoDto::getDto);
    }

    private Organization getOrganisation(Long organizationId) {
        return organizationRepository.findById(organizationId)
                .orElseThrow(OrganizationNotFoundException::new);
    }
}
