package webApplication.Etaskify.config;


import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import webApplication.Etaskify.security.AuthEntryPointJwt;
import webApplication.Etaskify.security.JwtUtils;
import webApplication.Etaskify.security.userDetails.UserDetailsImpl;

@TestConfiguration
public class TestControllerConfig {
    @Bean
    public UserDetailsImpl controllerService() {
        return new UserDetailsImpl(null, null, null, null);
    }

    @Bean
    public AuthEntryPointJwt bean() {
        return new AuthEntryPointJwt();
    }

    @Bean
    public JwtUtils jwtUtils() {
        return new JwtUtils();
    }
}
