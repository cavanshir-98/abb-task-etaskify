package webApplication.Etaskify.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import webApplication.Etaskify.exception.organization.OrganizationAlreadyExistException;
import webApplication.Etaskify.exception.organization.OrganizationNotFoundException;
import webApplication.Etaskify.mapper.OrganizationMapper;
import webApplication.Etaskify.model.Organization;
import webApplication.Etaskify.model.User;
import webApplication.Etaskify.repository.OrganizationRepository;
import webApplication.Etaskify.repository.UserRepository;
import webApplication.Etaskify.resource.organization.AddUserToOrgDto;
import webApplication.Etaskify.resource.organization.OrganizationCreateRequestDto;
import webApplication.Etaskify.resource.organization.OrganizationResponseInfoDto;
import webApplication.Etaskify.resource.organization.OrganizationSearchRequest;
import webApplication.Etaskify.service.impl.OrganizationServiceImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class OrganizationServiceImplTest {

    private static final String NAME = "ABB";
    private static final Long DUMMY_ID = 1L;
    private static final String ORGANIZATION_NOT_FOUND_MESSAGE = "Organization with id 1 not Found.";

    @Mock
    private OrganizationRepository organizationRepository;

    @Mock
    private OrganizationMapper organizationMapper;

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private OrganizationServiceImpl organizationService;

    @Mock
    Organization mockOrg;

    @Mock
    List<User> mocUsers;

    private Organization organization;

    @Mock
    private OrganizationSearchRequest organizationSearchRequest;

    @Mock
    private OrganizationCreateRequestDto organizationRequest;
    private OrganizationResponseInfoDto orgResponse;
    private AddUserToOrgDto registerOrgUse;
    private User user;

    @BeforeEach
    void setUp() {
        user = getUser();

        organization = getOrganization();

        organizationSearchRequest = getOrganizationSearchRequestDto();

        organizationRequest = getOrganizationRequestDto();

        orgResponse = getOrganizationResponseInfoDto();

        registerOrgUse = getRegisterOrgUse();
    }

    @Test
    public void givenRegisterOrganizationRequestDtoWhenCreateOrganizationThenSuccess() {
        //Arrange
        when(organizationRepository.save(organization)).thenReturn(organization);
        when(organizationMapper.organizationToDto(organization)).thenReturn(orgResponse);
        when(organizationRepository.findByName(NAME)).thenReturn(Optional.empty());
        //Act
        OrganizationResponseInfoDto result = organizationService.create(organizationRequest);
        //Assert
        assertThat(organizationRequest.getName()).isNotNull();
        assertThat(organization.getName()).isEqualTo(NAME);
        assertThat(result.getName()).isEqualTo(NAME);
        assertThat(result.getId()).isEqualTo(1L);
        verify(organizationRepository, times(1)).save(organization);
    }

    @Test
    public void givenRegisterOrganizationRequestDtoWhenCreateOrganizationThenException() {
        //Arrange
        when(organizationRepository.findByName(any())).thenReturn(Optional.of(organization));
        //Act & Assert
        assertThatThrownBy(() -> organizationService.create(organizationRequest))
                .isInstanceOf(OrganizationAlreadyExistException.class);
    }

    @Test
    public void organizationResponseSearchList() {
        //Arrange
        List<Organization> companies = new ArrayList<>();

        Pageable pageable = PageRequest.of(0, 5);
        Page<Organization> result = new PageImpl<>(companies, pageable, 1);

        when(organizationRepository.findAll(getOrganizationSearchRequestDto())).thenReturn(result);
        Page<OrganizationResponseInfoDto> list = organizationService.list(organizationSearchRequest);
        assertEquals(list.getTotalElements(), result.getTotalElements());
    }

    @Test
    void givenInValidIdAndValidServiceAddOrganizationUserWhenThenException() {
        //Arrange
        when(organizationRepository.findById(any())).thenReturn(Optional.of(mockOrg));
        when(mockOrg.getUser()).thenReturn(List.of(user));
        //Act & Assert
        assertThatThrownBy(() -> organizationService.addUserToOrganization(registerOrgUse, DUMMY_ID))
                .isInstanceOf(OrganizationAlreadyExistException.class);
    }

    @Test
    void givenInValidIdAndValidServiceAddOrganizationUserWhenThenReturnOk() {
        //Arrange
        when(userRepository.findById(any())).thenReturn(Optional.of(user));
        when(organizationRepository.findById(any())).thenReturn(Optional.of(mockOrg));
        when(mockOrg.getUser()).thenReturn(mocUsers);
        when(mocUsers.add(any())).thenReturn(true);
        //Act
        organizationService.addUserToOrganization(registerOrgUse, DUMMY_ID);
    }

    @Test
    void givenInValidIdAndValidServiceDeleteOrganizationUserOrganizationIdWhenUpdateThenException() {
        //Arrange
        when(organizationRepository.findById(any())).thenReturn(Optional.empty());
        //Act & Assert
        assertThatThrownBy(() -> organizationService.removeUserFromOrganization(registerOrgUse, DUMMY_ID))
                .isInstanceOf(OrganizationNotFoundException.class)
                .hasMessage(String.format(ORGANIZATION_NOT_FOUND_MESSAGE, DUMMY_ID));
    }

    @Test
    void givenInValidIdAndValidServiceDeleteOrganizationUserWhenUpdateThenReturnOk() {
        //Arrange
        when(userRepository.findById(any())).thenReturn(Optional.of(user));
        when(organizationRepository.findById(any())).thenReturn(Optional.of(mockOrg));
        when(mockOrg.getUser()).thenReturn(mocUsers);
        when(mocUsers.remove(any())).thenReturn(true);
        //Act
        organizationService.removeUserFromOrganization(registerOrgUse, DUMMY_ID);
    }

    private OrganizationCreateRequestDto getOrganizationRequestDto() {
        return OrganizationCreateRequestDto.builder()
                .name(NAME)
                .build();
    }

    private Organization getOrganization() {
        return Organization.builder()
                .name(NAME)
                .build();
    }

    private User getUser() {
        return User.builder()
                .id(1L)
                .email("test@gmail.com")
                .build();
    }

    private AddUserToOrgDto getRegisterOrgUse() {
        return AddUserToOrgDto.builder()
                .userId(1L)
                .build();
    }

    private OrganizationResponseInfoDto getOrganizationResponseInfoDto() {
        return OrganizationResponseInfoDto.builder()
                .id(1L)
                .name(NAME)
                .build();
    }

    private OrganizationSearchRequest getOrganizationSearchRequestDto() {
        return OrganizationSearchRequest.builder()
                .id(1L)
                .name("sss")
                .build();
    }
}
