package webApplication.Etaskify.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.exceptions.misusing.UnfinishedStubbingException;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.verification.VerificationMode;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import webApplication.Etaskify.enums.TaskStatus;
import webApplication.Etaskify.exception.NotFoundException;
import webApplication.Etaskify.model.Organization;
import webApplication.Etaskify.model.Task;
import webApplication.Etaskify.repository.OrganizationRepository;
import webApplication.Etaskify.repository.TaskRepository;
import webApplication.Etaskify.repository.UserRepository;
import webApplication.Etaskify.resource.organization.OrganizationResponseInfoDto;
import webApplication.Etaskify.resource.task.TaskCreateRequestDto;
import webApplication.Etaskify.resource.task.TaskResponseDto;
import webApplication.Etaskify.resource.task.TaskSearchRequest;
import webApplication.Etaskify.resource.task.TaskSearchResponseInfoDto;
import webApplication.Etaskify.service.impl.TaskServiceImpl;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class TaskServiceImplTest {

    private static final Long DUMMY_ID = 1L;
    private static final String DUMMY_STRING = "string";
    private static final LocalDate DUMMY_DATE = LocalDate.parse("2015-02-20");

    @InjectMocks
    private TaskServiceImpl taskService;

    @Mock
    private TaskRepository taskRepository;

    @Mock
    private UserRepository userRepository;

    @Mock
    private OrganizationRepository organizationRepository;

    @Spy
    private ModelMapper modelMapper;

    @Captor
    private ArgumentCaptor<Task> csCaptor;

    private TaskCreateRequestDto taskRequestDto;
    private TaskSearchRequest taskSearchRequest;
    private TaskResponseDto taskResponseDto;
    private Organization organization;

    @Mock
    private Task task;

    @BeforeEach
    void setUp() {
        taskRequestDto = getTaskRequestDto();

        taskResponseDto = getTaskResponseDto();

        organization = createOrganization();

        taskSearchRequest = getTaskSearhRequest();

        task = getTask();
    }

    private TaskSearchRequest getTaskSearhRequest() {
        return TaskSearchRequest.builder()
                .name(DUMMY_STRING)
                .build();
    }

    @Test
    void givenTaskRequestDtoWhenCreateThenTaskResponseDto() {
        //Assert
        assertThat(taskRequestDto.getDescription()).isNotNull();
        assertThat(task.getTitle()).isEqualTo(DUMMY_STRING);
    }

    @Test
    public void givenRegisterOrganizationRequestDtoWhenCreateOrganizationThenException() {
        //Arrange
        when(taskRepository.findByTitle(anyString()));
        //Act & Assert
        assertThatThrownBy(() -> taskService.create(taskRequestDto))
                .isInstanceOf(UnfinishedStubbingException.class);
    }

    @Test
    void givenInValidTaskIdWhenDeleteThenException() {
        //Act & Assert
        assertThatThrownBy(() -> taskService.delete(DUMMY_ID)).isInstanceOf(NotFoundException.class);
    }

    @Test
    void givenValidTaskIdWhenDeleteThenOk() {
        when(taskRepository.findById(any(Long.class))).thenReturn(Optional.of(task));
        //Act
        taskService.delete(DUMMY_ID);
        //Verify
        verify(taskRepository, times(1)).delete(any(Task.class));
    }

    @Test
    public void taskResponseSearchList() {
        //Arrange
        List<Task> companies = new ArrayList<>();

        Pageable pageable = PageRequest.of(0, 5);
        Page<Task> result = new PageImpl<>(companies, pageable, 1);

        when(taskRepository.findAll(getTaskSearhRequest())).thenReturn(result);

        Page<TaskSearchResponseInfoDto> list = taskService.list(taskSearchRequest);
        assertEquals(list.getTotalElements(), result.getTotalElements());
    }

    private Organization createOrganization() {
        return Organization.builder()
                .id(DUMMY_ID)
                .name(DUMMY_STRING)
                .build();
    }

    private TaskResponseDto getTaskResponseDto() {
        return TaskResponseDto
                .builder()
                .id(DUMMY_ID)
                .title(DUMMY_STRING)
                .description(DUMMY_STRING)
                .status(TaskStatus.ONGOING)
                .deadline(DUMMY_DATE)
                .organization(OrganizationResponseInfoDto.builder()
                        .id(DUMMY_ID)
                        .name(DUMMY_STRING)
                        .build())
                .build();
    }

    private Task getTask() {
        return Task
                .builder()
                .id(DUMMY_ID)
                .title(DUMMY_STRING)
                .description(DUMMY_STRING)
                .status(TaskStatus.ONGOING)
                .deadline(DUMMY_DATE)
                .organization(Organization.builder()
                        .id(DUMMY_ID)
                        .name(DUMMY_STRING)
                        .build())
                .build();
    }

    private TaskCreateRequestDto getTaskRequestDto() {
        return TaskCreateRequestDto
                .builder()
                .title(DUMMY_STRING)
                .description(DUMMY_STRING)
                .status(TaskStatus.ONGOING)
                .deadline(DUMMY_DATE)
                .organizationId(DUMMY_ID)
                .build();
    }
}
