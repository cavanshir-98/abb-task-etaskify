package webApplication.Etaskify.service;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import webApplication.Etaskify.enums.RoleEnum;
import webApplication.Etaskify.exception.EmailAlreadyExistException;
import webApplication.Etaskify.mapper.UserMapper;
import webApplication.Etaskify.model.Role;
import webApplication.Etaskify.model.User;
import webApplication.Etaskify.repository.RoleRepository;
import webApplication.Etaskify.repository.UserRepository;
import webApplication.Etaskify.resource.user.RegisterUserDto;
import webApplication.Etaskify.resource.user.UserDto;
import webApplication.Etaskify.service.impl.UserServiceImpl;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class UserServiceImplTest {

    private static final String EMAIL = "mail@mail.ru";
    private static final String NAME = "First";
    private static final String SURNAME = "Last";
    private static final String PASSWORD = "102030Aa*";

    @Mock
    private UserRepository userRepository;
    @Mock
    private PasswordEncoder passwordEncoder;
    @Mock
    private UserMapper userMapper;

    @Mock
    AuthenticationManager authenticationManager;

    @Mock
    RoleRepository authRepository;
    @InjectMocks
    private UserServiceImpl userService;

    private User user;
    private RegisterUserDto registerUserDto;
    private UserDto userDto;

    @BeforeEach
    public void setUp() {

        user = User.builder()
                .id(1L)
                .email(EMAIL)
                .name(NAME)
                .build();

        registerUserDto = RegisterUserDto.builder()
                .email(EMAIL)
                .password(PASSWORD)
                .name(NAME)
                .build();

        userDto = UserDto.builder()
                .name(NAME)
                .surname(SURNAME)
                .email(EMAIL)
                .build();

    }

    @Test
    public void givenRegisterUserDtoWhenCreateUserThenSuccess() {
        //Arrange
        when(userRepository.findByEmail(any())).thenReturn(Optional.empty());
        when(authRepository.findByName(any())).thenReturn(Optional.of(Role.builder()
                .name(RoleEnum.USER)
                .build()));
        when(userRepository.save(any())).thenReturn(user);
        //Act
        userService.create(registerUserDto);
        //Assert
        assertThat(user.getRoles()).isNotNull();
        assertThat(user.getEmail()).isEqualTo(EMAIL);
        assertThat(user.getName()).isEqualTo(NAME);
    }

    @Test
    public void givenRegisterUserDtoWithAlreadyRegisteredEmailWhenCreateUserThenError() {
        //Arrange
        when(userRepository.findByEmail(any())).thenReturn(Optional.of(user));
        //Act & Assert
        assertThatThrownBy(() -> userService.create(registerUserDto)).isInstanceOf(EmailAlreadyExistException.class);
    }
}
