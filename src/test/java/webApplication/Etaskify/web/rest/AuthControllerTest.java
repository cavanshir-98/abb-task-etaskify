package webApplication.Etaskify.web.rest;

import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.ResponseEntity;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;
import webApplication.Etaskify.config.TestControllerConfig;
import webApplication.Etaskify.controller.AuthController;
import webApplication.Etaskify.model.Organization;
import webApplication.Etaskify.model.User;
import webApplication.Etaskify.repository.OrganizationRepository;
import webApplication.Etaskify.repository.RoleRepository;
import webApplication.Etaskify.repository.UserRepository;
import webApplication.Etaskify.resource.token.InteractionResponse;
import webApplication.Etaskify.resource.user.LoginCreateRequestDto;
import webApplication.Etaskify.resource.user.RegisterRequest;
import webApplication.Etaskify.security.userDetails.UserDetailsServiceImpl;
import webApplication.Etaskify.service.impl.AuthServiceImpl;

import java.util.List;
import java.util.Set;

import static org.mockito.Mockito.when;

@WithMockUser
@AutoConfigureMockMvc
@ExtendWith(MockitoExtension.class)
@WebMvcTest(AuthController.class)
@Import({TestControllerConfig.class})
public class AuthControllerTest {

    @MockBean
    private UserDetailsServiceImpl userDetailsService;

    @MockBean
    private AuthServiceImpl authService;

    @Mock
    private UserRepository userRepository;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Mock
    private OrganizationRepository organizationRepository;

    @Mock
    private RoleRepository authRepository;

    private Organization organization;

    private LoginCreateRequestDto loginCreateRequestDto;
    private RegisterRequest registerRequestDto;
    private User user;

    @Mock
    Set<User> mocUsers;

    private static final String DUMMY_STRING = "string";
    private static final String EMAIL = "test@test.com";
    private static final Long DUMMY_ID = 1L;
    private static final String ROLE_ADMIN = "ADMIN";

    @Before
    public void setup() {
        user = User.builder()
                .id(DUMMY_ID)
                .password(DUMMY_STRING)
                .email(EMAIL)
                .name(DUMMY_STRING)
                .organization(Organization.builder()
                        .name(DUMMY_STRING)
                        .build())
                .build();

        organization = Organization.builder()
                .name(DUMMY_STRING)
                .build();

        registerRequestDto = RegisterRequest.builder()
                .username(DUMMY_STRING)
                .password(DUMMY_STRING)
                .organizationName(DUMMY_STRING)
                .authority(List.of("ADMIN"))
                .email(EMAIL)
                .build();

        loginCreateRequestDto = LoginCreateRequestDto.builder()
                .email(DUMMY_STRING)
                .password(DUMMY_STRING)
                .build();
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    public void registerUser() {
        ResponseEntity<InteractionResponse> ok = ResponseEntity.ok(new InteractionResponse("User registered successfully!"));
        AuthController authController = new AuthController(authService);
        authController.registerUser(registerRequestDto);
        //Arrange
        when(authService.register(registerRequestDto)).thenReturn(user);
        //Assert
    }

    @Test
    public void loginForm_ShouldIncludeNewUserInModel() throws Exception {
        AuthController authController = new AuthController(authService);
        authController.login(loginCreateRequestDto);

        when(authService.login(loginCreateRequestDto)).thenReturn(null);
    }
}
