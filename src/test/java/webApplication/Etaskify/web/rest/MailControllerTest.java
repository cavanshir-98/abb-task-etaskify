package webApplication.Etaskify.web.rest;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import webApplication.Etaskify.controller.MailController;
import webApplication.Etaskify.security.AuthEntryPointJwt;
import webApplication.Etaskify.security.JwtUtils;
import webApplication.Etaskify.security.userDetails.UserDetailsServiceImpl;
import webApplication.Etaskify.service.impl.MailServiceImpl;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@WithMockUser
@AutoConfigureMockMvc
@ExtendWith(SpringExtension.class)
@WebMvcTest(MailController.class)
class MailControllerTest {

    @MockBean
    private MailServiceImpl mailService;

    @MockBean
    private UserDetailsServiceImpl userDetailsService;

    @MockBean
    private AuthEntryPointJwt authEntryPointJwt;

    @MockBean
    private JwtUtils jwtUtils;

    @Test
    void mailController() {
        verify(mailService, times(0)).sendWithoutAttachment("xeyal.aqayev1998@gmail.com", "cavansir.asad@gmail.com", "testContent");
    }
}
