package webApplication.Etaskify.web.rest;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import webApplication.Etaskify.config.TestControllerConfig;
import webApplication.Etaskify.controller.OrganizationController;
import webApplication.Etaskify.resource.organization.AddUserToOrgDto;
import webApplication.Etaskify.resource.organization.OrganizationCreateRequestDto;
import webApplication.Etaskify.resource.organization.OrganizationResponseInfoDto;
import webApplication.Etaskify.resource.organization.OrganizationSearchRequest;
import webApplication.Etaskify.security.userDetails.UserDetailsServiceImpl;
import webApplication.Etaskify.service.OrganizationService;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@WithMockUser
@AutoConfigureMockMvc
@ExtendWith(MockitoExtension.class)
@WebMvcTest(OrganizationController.class)
@Import({TestControllerConfig.class})
class OrganizationControllerTest {

    private static final String MAIN_URL = "/organizations";
    private static final String NAME = "ABB";
    private static final Long DUMMY_ID = 1L;

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @MockBean
    private OrganizationService orgService;

    @Mock
    private ObjectMapper mapper;

    @Mock
    private OrganizationSearchRequest organizationSearchRequest;

    @MockBean
    private UserDetailsServiceImpl userDetailsService;

    @Mock
    private OrganizationCreateRequestDto organizationCreateRequestDto;
    private OrganizationResponseInfoDto orgResponseInfoDto;
    private AddUserToOrgDto addUserToOrgDto;

    @BeforeEach
    void setUp() {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(this.webApplicationContext)
                .build();

        organizationCreateRequestDto = getOrgRequestDto();

        orgResponseInfoDto = getOrgResponseInfoDto();

        organizationSearchRequest = getOrganizationSearchRequestDto();

        addUserToOrgDto = getRegisterOrgUse();
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    public void getOrganizationList() {
        //Arrange
        when(orgService.list(getOrganizationSearchRequestDto())).thenReturn(null);
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    public void givenRegisterOrgRequestDtoWhenCreateOrgThenSuccess() {
        //Arrange
        when(orgService.create(organizationCreateRequestDto)).thenReturn(orgResponseInfoDto);
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void givenValidIdWhenDeleteOrgUserThenNoContent() {
        //Arrange
        doNothing().when(orgService).removeUserFromOrganization(addUserToOrgDto, DUMMY_ID);
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void givenValidIdWhenAddOrgUserThenCreated() throws Exception {
        //Arrange
        doNothing().when(orgService).addUserToOrganization(addUserToOrgDto, DUMMY_ID);
        //Act
        mockMvc.perform(post(MAIN_URL + "/user/" + DUMMY_ID));
    }

    private String convertObjectToString(Object obj) throws JsonProcessingException {
        return mapper.writeValueAsString(obj);
    }

    private AddUserToOrgDto getRegisterOrgUse() {
        return AddUserToOrgDto.builder()
                .userId(1L)
                .build();
    }

    private OrganizationResponseInfoDto getOrgResponseInfoDto() {
        return OrganizationResponseInfoDto.builder()
                .id(1L)
                .name(NAME)
                .build();
    }

    private OrganizationCreateRequestDto getOrgRequestDto() {
        return OrganizationCreateRequestDto.builder()
                .name(NAME)
                .build();
    }

    private OrganizationSearchRequest getOrganizationSearchRequestDto() {
        return OrganizationSearchRequest.builder()
                .id(1L)
                .name("sss")
                .build();
    }
}
