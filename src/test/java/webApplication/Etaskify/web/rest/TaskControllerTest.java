package webApplication.Etaskify.web.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import webApplication.Etaskify.config.TestControllerConfig;
import webApplication.Etaskify.controller.TaskController;
import webApplication.Etaskify.enums.TaskStatus;
import webApplication.Etaskify.resource.organization.OrganizationResponseInfoDto;
import webApplication.Etaskify.resource.task.TaskCreateRequestDto;
import webApplication.Etaskify.resource.task.TaskResponseDto;
import webApplication.Etaskify.security.userDetails.UserDetailsServiceImpl;
import webApplication.Etaskify.service.TaskService;

import java.time.LocalDate;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@AutoConfigureMockMvc
@ExtendWith(SpringExtension.class)
@WebMvcTest(TaskController.class)
@Import({TestControllerConfig.class})
class TaskControllerTest {

    private static final Long DUMMY_ID = 1L;
    private static final String DUMMY_STRING = "string";
    private static final String BASE_URL = "/tasks";
    private static final String ROLE_ADMIN = "ADMIN";
    private static final LocalDate DUMMY_DATE = LocalDate.parse("2015-02-20");

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private TaskService taskService;

    @MockBean
    private UserDetailsServiceImpl userDetailsService;

    private TaskResponseDto taskResponseDto;
    private TaskCreateRequestDto taskCreateRequestDto;

    @BeforeEach
    void setUp() {

        taskResponseDto = TaskResponseDto
                .builder()
                .id(DUMMY_ID)
                .title(DUMMY_STRING)
                .description(DUMMY_STRING)
                .status(TaskStatus.ONGOING)
                .deadline(LocalDate.from(DUMMY_DATE))
                .organization(OrganizationResponseInfoDto.builder()
                        .id(DUMMY_ID)
                        .name(DUMMY_STRING)
                        .build())
                .build();

        taskCreateRequestDto = TaskCreateRequestDto
                .builder()
                .title(DUMMY_STRING)
                .description(DUMMY_STRING)
                .status(TaskStatus.ONGOING)
                .deadline(DUMMY_DATE)
                .organizationId(DUMMY_ID)
                .build();
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void givenValidInputWhenCreateThenReturnOk() throws Exception {
        //Arrange
        when(taskService.create(taskCreateRequestDto)).thenReturn(taskResponseDto);
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void givenValidIdWhenDeleteThenNoContent() throws Exception {
        //Action
        ResultActions actions = mockMvc.perform(delete(BASE_URL + "/" + DUMMY_ID)
                .contentType(MediaType.APPLICATION_JSON));
        //Assert
        verify(taskService, times(1)).delete(DUMMY_ID);
        actions.andExpect(status().isNoContent());
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void givenValidInputWhenSearchThenOk() throws Exception {
        //Arrange
        final Page<TaskResponseDto> payerDtoPage = Page.empty();
        //Act
        mockMvc.perform(post(BASE_URL + "/search")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(payerDtoPage)));
    }
}
